﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MessageText : MonoBehaviour {
	public Text _TextBox;
	public Animator _anim;
	public bool animFlag;
	public bool disanimflag;

	// Use this for initialization
	void Start () {
		animFlag = false;
		disanimflag =false;
		_anim = this.GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
	if(animFlag){
			animFlag = false;
			_anim.SetBool ("AppearFlag",true);
			_anim.SetBool("DisAppearFlag",false);
		}
	if(disanimflag){
			disanimflag = false;
			_anim.SetBool ("AppearFlag",false);
			_anim.SetBool("DisAppearFlag",true);
		}

	}
}
