﻿using UnityEngine;
using System.Collections;

public class SelectYesorNo : MonoBehaviour {
	public bool selectYes=true;
	public GameObject Allow_A;
	public GameObject Allow_B;

	// Use this for initialization
	void Start () {
		selectYes = true;
		Allow_A.SetActive (true);
		Allow_B.SetActive (false);
	}
	
	// Update is called once per frame
	void Update () {
	if (Mathf.Abs(Input.GetAxis ("Horizontal"))>0) {
			if(Input.GetAxis ("Horizontal")<0){
				Allow_A.SetActive (true);
				Allow_B.SetActive (false);
				selectYes = true;

			}else if(Input.GetAxis ("Horizontal")>0){
				Allow_A.SetActive (false);
				Allow_B.SetActive (true);
				selectYes = false;
			}
	}
	}
}
