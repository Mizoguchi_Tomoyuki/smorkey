﻿using UnityEngine;
using System.Collections;

public class PlayerState : MonoBehaviour {
	public enum PLAYER_STATE{
		GROUND = 0,
		FLOAT = 1,
		BLOW = 2,
		EVENT = 3,
	};
	public enum PLAYER_ACTION
		{
		NORMAL = 0,
		SMOKE = 1,
		FALL = 2,
		WALK = 3,
		SQUATE = 4,
	};
	public LayerMask groundLayer;
	public LayerMask blowLayer;
	public PLAYER_STATE player_state;
	public PLAYER_ACTION player_action;
	public bool player_forward = true;
	public bool bodydown, bodyup;
	public bool lineflag=false;
	public Vector3 _bodyUp,_bodyDown,_blowCheck,_GroundCheck;
	private Vector2 _bodyUp2D, _bodyDown2D, _blowCheck2D, _GroundCheck2D;
	private GameObject[] LineofRay;
	private LineRenderer[] line;
	public bool hit_box_adjust=false;
	private float hit_height_stand = 2.137743f;
	private float hit_height_squate = 0.9566016f;
	private float hit_width_stand = 1.006843f;
	private float hit_width_center_stand = -0.001279235f;
	private float hit_width_squate = 2.998424f;
	private float hit_width_center_squate = -0.008768559f;
	private float hit_center_squate = 0.02f;
	private float hit_center_stand = 0.6063278f;
	private BoxCollider2D hit_box;

	public PlayerSpriteChanger _playerspriteChanger;

	// Use this for initialization
	void Start () {
		LineofRay = new GameObject[4];
		line = new LineRenderer[4];
		if(lineflag){
			for(int i=0;i<LineofRay.Length;i++){
				LineofRay[i] = new GameObject();
				LineofRay[i].transform.parent = this.transform;
				LineofRay[i].AddComponent("LineRenderer");
				line[i] = LineofRay[i].GetComponent<LineRenderer>();
			}
		}
		hit_box = this.GetComponent<BoxCollider2D> ();
		if (hit_box_adjust) {
			hit_height_stand = hit_box.size.y;
			hit_center_stand = hit_box.center.y;
			
			hit_height_squate = hit_height_stand/2f;
			hit_center_stand = hit_box.center.y-hit_height_stand/4f;
		}
	}
	
	// Update is called once per frame
	void Update () {
		//接地処理.Laycastを下方向に向けることで接地判定. ----------
		Vector2 position2D = new Vector2 (transform.position.x, transform.position.y);
		 _GroundCheck2D = new Vector2 (_GroundCheck.x, _GroundCheck.y);
		bool grounded = Physics2D.Linecast(
			position2D + _GroundCheck2D + Vector2.up * _GroundCheck.z,
			position2D + _GroundCheck2D - Vector2.up * _GroundCheck.z,
			groundLayer
			);
		_blowCheck2D = new Vector2 (_blowCheck.x, _blowCheck.y);
		bool blowup = Physics2D.Linecast(
			position2D + _blowCheck2D + Vector2.up*_blowCheck.z,
			position2D + _blowCheck2D - Vector2.up*_blowCheck.z,
			blowLayer
			);
		//屈伸判定. ----------
		Vector3 _bodytmpUp,_bodytmpDown;
		if(!player_forward){
			_bodytmpUp = new Vector3(-_bodyUp.x,_bodyUp.y,_bodyUp.z);
			_bodytmpDown = new Vector3(-_bodyDown.x,_bodyDown.y,_bodyDown.z);
		}else{
			_bodytmpUp = new Vector3(_bodyUp.x,_bodyUp.y,_bodyUp.z);
			_bodytmpDown = new Vector3(_bodyDown.x,_bodyDown.y,_bodyDown.z);
		}
		_bodyUp2D = new Vector2 (_bodytmpUp.x, _bodytmpUp.y);
		_bodyDown2D = new Vector2 (_bodytmpDown.x, _bodytmpDown.y);
		bodyup = Physics2D.Linecast(
			position2D + _bodyUp2D + Vector2.right*_bodytmpUp.z,
			position2D + _bodyUp2D - Vector2.right*_bodytmpUp.z,
			groundLayer
			);
		bodydown = Physics2D.Linecast(
			position2D + _bodyDown2D + Vector2.right*_bodytmpDown.z,
			position2D + _bodyDown2D - Vector2.right*_bodytmpDown.z,
			groundLayer
			);

		//各判定の状態に対する処理. 
		if (grounded) { //接地判定. 
						if(player_state==PLAYER_STATE.FLOAT){
						_playerspriteChanger.ChangeAnim("Fallend");
						}
						player_state = PLAYER_STATE.GROUND;
				} else {
			
			if (blowup) { //吹上判定. 
				player_state = PLAYER_STATE.BLOW;
				if(player_action == PLAYER_ACTION.SMOKE){
				rigidbody2D.gravityScale = -0.8f;
				}
			}else{
				if(player_state == PLAYER_STATE.BLOW){//吹上エリアを抜け重力をもとに戻す判定に用いる. 
					if(player_action == PLAYER_ACTION.SMOKE){
					rigidbody2D.gravityScale = 0f;
					}else{
						rigidbody2D.gravityScale = 1.0f;
					}
				}
				if(player_action != PLAYER_ACTION.SMOKE){
				_playerspriteChanger.ChangeAnim("Fall");
				}
				player_state = PLAYER_STATE.FLOAT; //BLOW状態でないなら接地していなければFLOAT状態に. 
			}
		}
		if (player_action == PLAYER_ACTION.SQUATE ) {//屈伸状態に関する処理.
			hit_box.size = new Vector2 (hit_width_squate, hit_height_squate);
						hit_box.center = new Vector2 (hit_width_center_squate, hit_center_squate);
			}else if(player_action == PLAYER_ACTION.SMOKE){
					hit_box.size = new Vector2 (hit_width_squate/2f, hit_height_squate);
					hit_box.center = new Vector2 (hit_width_center_squate, hit_center_squate);
			} else {
			
			hit_box.size = new Vector2(hit_width_stand,hit_height_stand);
			hit_box.center = new Vector2(hit_width_center_stand,hit_center_stand);
				}
		//-----------------------------------------------------
		if (lineflag) {
						LineSetter ();
		}
		if (SmokeGameManager.eventprog == SmokeGameManager.EVENTPROG.START) { //イベントに入ったらアクションを記憶する処理. 

		}
		if(SmokeGameManager.eventprog == SmokeGameManager.EVENTPROG.PLAY) {
			player_state = PLAYER_STATE.EVENT;
		}
		if (SmokeGameManager.eventprog == SmokeGameManager.EVENTPROG.END) {
			if(player_state == PLAYER_STATE.EVENT){
			}
		}

	}

	public void SetPlayer_Action(PLAYER_ACTION pl_act){
		player_action = pl_act;
	}

	public void LineSetter(){
		
		Vector2 position2D = new Vector2 (transform.position.x, transform.position.y);

		line[0].SetPosition(0, position2D + _GroundCheck2D + Vector2.up * _GroundCheck.z);
		line[0].SetPosition(1, position2D + _GroundCheck2D - Vector2.up * _GroundCheck.z);
		line[0].material = new Material(Shader.Find ("Sprites/Default"));
		line[0].SetColors(Color.red,Color.red);
		line [0].SetWidth (0.1f, 0.1f);

		line[1].SetPosition(0, position2D + _blowCheck2D + Vector2.up*_blowCheck.z);
		line[1].SetPosition(1, position2D + _blowCheck2D - Vector2.up*_blowCheck.z);
		line[1].material = new Material(Shader.Find ("Sprites/Default"));
		line[1].SetColors(Color.blue,Color.blue);
		line [1].SetWidth (0.1f, 0.1f);

		
		Vector3 _bodytmpUp,_bodytmpDown;
		if(!player_forward){
			_bodytmpUp = new Vector3(-_bodyUp.x,_bodyUp.y,_bodyUp.z);
			_bodytmpDown = new Vector3(-_bodyDown.x,_bodyDown.y,_bodyDown.z);
		}else{
			_bodytmpUp = new Vector3(_bodyUp.x,_bodyUp.y,_bodyUp.z);
			_bodytmpDown = new Vector3(_bodyDown.x,_bodyDown.y,_bodyDown.z);
		}


		line[2].SetPosition(0, position2D + _bodyUp2D + Vector2.right*_bodytmpUp.z);
		line[2].SetPosition(1, position2D + _bodyUp2D - Vector2.right*_bodytmpUp.z);
		line[2].material = new Material(Shader.Find ("Sprites/Default"));
		line[2].SetColors(Color.yellow,Color.yellow);
		line [2].SetWidth (0.1f, 0.1f);
		
		line[3].SetPosition(0, position2D + _bodyDown2D + Vector2.right*_bodytmpDown.z);
		line[3].SetPosition(1, position2D + _bodyDown2D - Vector2.right*_bodytmpDown.z);
		line[3].material = new Material(Shader.Find ("Sprites/Default"));
		line[3].SetColors(Color.green,Color.green);
		line [3].SetWidth (0.1f, 0.1f);


	}
}
