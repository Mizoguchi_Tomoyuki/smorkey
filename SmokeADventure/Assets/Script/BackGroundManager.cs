﻿using UnityEngine;
using System.Collections;

public class BackGroundManager : MonoBehaviour {
	public GameObject[] BackGrounds;
	public Material SpriteMaterial;
	public int NowLayer = 0;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	public void ChangeBackGroundLayer(int layer){
		BackGrounds[layer].SetActive(true);
		BackGrounds[NowLayer].SetActive(false);
		NowLayer = layer;

	}
}
