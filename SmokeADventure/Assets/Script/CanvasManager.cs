﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CanvasManager : MonoBehaviour {
	public Image TextZonePanelImage;
	public Text TextZoneText;
	public GameObject Selecter;
	public MessageText message_text;
	// Use this for initialization
	void Start () {
		Selecter.SetActive (false);
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	public void TextZoneAppear(){
		TextZonePanelImage.color = new Color(TextZonePanelImage.color.r,TextZonePanelImage.color.g,TextZonePanelImage.color.b,0.7f);
		TextZoneText.color = new Color(TextZonePanelImage.color.r,TextZonePanelImage.color.g,TextZonePanelImage.color.b,1.0f);
		if(message_text != null){
			message_text.animFlag = true;
		}
	}
	public void TextZoneDisAppear(){
		TextZonePanelImage.color = new Color(TextZonePanelImage.color.r,TextZonePanelImage.color.g,TextZonePanelImage.color.b,0.0f);
		TextZoneText.color = new Color(TextZonePanelImage.color.r,TextZonePanelImage.color.g,TextZonePanelImage.color.b,0.0f);
		if(message_text != null){
			message_text.disanimflag = true;
		}
	}
	public void TextZoneTextSet(string tex){
		TextZoneText.text = tex;
	}

	
	public void SelecterAppear(){
		Selecter.SetActive (true);
	}
	public bool GetSelecterChoice(){
		SelectYesorNo sl = Selecter.GetComponent<SelectYesorNo> ();
		return sl.selectYes;
	}
	public void SelecterDisAppear(){
		Selecter.SetActive (false);
	}
}
