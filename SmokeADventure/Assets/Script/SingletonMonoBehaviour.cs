﻿using UnityEngine;
//MonoBehaviourを継承したジェネリックなシングルトンを作りInstanceでの処理としてすでにある場合は作らない. 
public class SingletonMonoBehaviour<T> : MonoBehaviour where T : MonoBehaviour
{
	private static T instance;
	public static T Instance {
		get {
			if (instance == null) {
				instance = (T)FindObjectOfType(typeof(T));
				
				if (instance == null) {
					Debug.LogError (typeof(T) + "is nothing");
				}
			}
			
			return instance;
		}
	}
	
}