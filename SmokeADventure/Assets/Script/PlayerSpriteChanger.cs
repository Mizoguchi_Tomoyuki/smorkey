﻿using UnityEngine;
using System.Collections;

public class PlayerSpriteChanger : MonoBehaviour {
	public PlayerState player;
	public Sprite NormalSprite;
	public Sprite SquateSprite;
	public Sprite SmokeSprite;
	public Animator _playerAnim;
	private SpriteRenderer SPRend;
	private int switcher = 1;
	// Use this for initialization
	void Start () {
		SPRend = this.GetComponent<SpriteRenderer> ();
	}
	
	// Update is called once per frame
	void Update () {
	/*	PlayerState.PLAYER_ACTION pl_action = player.player_action;
	switch (pl_action) {
		case PlayerState.PLAYER_ACTION.NORMAL:
			SPRend.sprite = NormalSprite;
			break;
		case PlayerState.PLAYER_ACTION.SQUATE:
			SPRend.sprite = SquateSprite;
			break;
		case PlayerState.PLAYER_ACTION.SMOKE:
			SPRend.sprite = SmokeSprite;
			break;
		default:
			break;
	}*/

		if (player.player_forward) {
			if(switcher == 1){
				switcher = -1;
				this.transform.localScale = new Vector3 (this.transform.localScale.x * switcher, this.transform.localScale.y, this.transform.localScale.z);
			}
		} else if(!player.player_forward){
			if(switcher == -1){
				switcher = 1;
				this.transform.localScale = new Vector3 (this.transform.localScale.x * (-1), this.transform.localScale.y, this.transform.localScale.z);
			}
		}
	}

	public void ChangeAnim(string Anim){
		_playerAnim.SetTrigger (Anim);
	}
}
