﻿using UnityEngine;
using System.Collections;

public class CameraRange : MonoBehaviour {
	
	#if UNITY_EDITOR
	
	float depth = 10;
	public bool drawSubline = true;
	
	Vector3 leftTop, rightDown, rightTop, leftDown, forwardPosition;
	
	private Vector3 CamNearFocus = new Vector3(0,1.78f,-10);
	private Vector3 CamNormalFocus = new Vector3(0.31f,2.65f,-10);
	private Vector3 CamFarFocus = new Vector3(0,6.24f,-10);
	public float width,height;
	
	private float NormalWidth = 17.21f;
	private float NormalHeigt = 9.98f;
	public enum Focus{
		NEAR=0,
		NORMAL=1,
		FAR=2,

	};
	public Focus _focus;
	void PositionUpdate(float z)
	{
		Vector3 CamFocus = Vector3.zero;
		float Width = 0;
		float Height = 0;
		switch(_focus){
		case Focus.NORMAL:
			Width = NormalWidth;
			Height = NormalHeigt;
			CamFocus = CamNormalFocus;
			break;
		case Focus.NEAR:
			Width = (NormalWidth*2.5f)/5f;
			Height = (NormalHeigt*2.5f)/5f;
			CamFocus = CamNearFocus;
			break;
		case Focus.FAR:
			Width = (NormalWidth*7f)/5f;
			Height = (NormalHeigt*7f)/5f;
			CamFocus = CamFarFocus;
			break;
		}
		leftTop = new Vector3(this.transform.position.x-0.5f*Width+CamFocus.x, this.transform.position.y+0.5f*Height+CamFocus.y, z-10);
		rightDown = new Vector3(this.transform.position.x+0.5f*Width+CamFocus.x, this.transform.position.y-0.5f*Height+CamFocus.y, z-10);
		rightTop = new Vector3(this.transform.position.x+0.5f*Width+CamFocus.x, this.transform.position.y+0.5f*Height+CamFocus.y, z-10);
		leftDown = new Vector3(this.transform.position.x-0.5f*Width+CamFocus.x, this.transform.position.y-0.5f*Height+CamFocus.y, z-10);
	}
	
	void OnDrawGizmos()
	{
		
		Vector3 forwardPosition =  Vector3.forward * depth;
		PositionUpdate( forwardPosition.z);

		
		Gizmos.DrawLine(leftTop, rightTop);
		Gizmos.DrawLine(leftTop, leftDown);
		Gizmos.DrawLine(rightDown, leftDown);
		Gizmos.DrawLine(rightDown, rightTop);
		
		Gizmos.color = new Color(1, 1, 1, 0.4f);
	/*	
		if(! drawSubline )
			return;
		
		if( !targetCamera.orthographic )
		{
			Gizmos.DrawLine(cameraPosition, (leftTop - cameraPosition) * 10+ cameraPosition); 
			Gizmos.DrawLine(cameraPosition, (leftDown - cameraPosition) * 10+ cameraPosition); 
			Gizmos.DrawLine(cameraPosition, (rightTop - cameraPosition) * 10+ cameraPosition); 
			Gizmos.DrawLine(cameraPosition, (rightDown - cameraPosition) * 10+ cameraPosition); 
		}else{
			Vector3 forward = targetCamera.transform.forward * 10;
			Vector3 back = targetCamera.transform.forward * -10;
			Gizmos.DrawLine(leftTop + forward, leftTop + back); 
			Gizmos.DrawLine(leftDown + forward, leftDown + back); 
			Gizmos.DrawLine(rightTop + forward, rightTop + back); 
			Gizmos.DrawLine(rightDown + forward, rightDown + back); 
		}*/
	}
	
	void OnDrawGizmosSelected()
	{
	/*	Camera targetCamera = camera;
		depth = -targetCamera.transform.localPosition.z;
		*/
		Vector3 leftcenter = new Vector3(1, 0.5f, forwardPosition.z);
		Vector3 centerTop = new Vector3(0.5f, 0, forwardPosition.z);
		
		UnityEditor.Handles.Label(leftTop, leftTop.ToString());
		UnityEditor.Handles.Label(rightDown, rightDown.ToString());
		
		UnityEditor.Handles.Label(leftcenter, Vector3.Distance(leftTop, leftDown).ToString("0.0"));
		UnityEditor.Handles.Label(centerTop, Vector3.Distance(leftTop, rightTop).ToString("0.0"));
		
	}
	
	void OnValidate()
	{
		depth = Mathf.Max(0, depth);
	}
	#endif
}