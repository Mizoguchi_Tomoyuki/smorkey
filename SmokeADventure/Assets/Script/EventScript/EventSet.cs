﻿using UnityEngine;
using System.Collections;

public class EventSet : MonoBehaviour {
	public GameObject PlayerObj;
	public EventManager.EventType _eventType;
	public int priority=0;
	public bool ForceFlag=false;
	public LayerMask playerLayer;
	private bool eventflag = false;

	private Color RED = Color.red;
	private Color Original;
	private bool selected = false;
	private SpriteRenderer SpRend;
	private float BlingDown=0;
	private bool turnflag = false;
	private bool preselect = false;
	public bool EventAreaVisible=false;
	[HideInInspector]
	public Vector2 EventArea_Center;
	[HideInInspector]
	public float EventArea_Length;
	[HideInInspector]
	public float EventDistance=5;

	private LineRenderer _Line;
	// Use this for initialization
	void Start () {
		RED = new Color (175f / 255f, 94f / 255f, 81f / 255f, 0.9f);
		SpRend = GetComponent<SpriteRenderer> ();

		Original = SpRend.color;
		if (EventAreaVisible) {
			this.gameObject.AddComponent("LineRenderer");
			_Line = GetComponent<LineRenderer>();
			_Line.useWorldSpace = true;
		}
	}
	
	// Update is called once per frame
	void Update () {
		float distance = Vector2.Distance(new Vector2 (this.transform.position.x, this.transform.position.y)+ EventArea_Center, new Vector2 (PlayerObj.transform.position.x, PlayerObj.transform.position.y));
		if (distance < EventDistance) {
						selected = EventAreaScan ();
				}
		if (selected) {
			
			float Tmppriority = priority;
			if(EventManager.Instance){
				eventflag = EventManager.Instance.EventAdd(Tmppriority,this.gameObject);
			}
			if(eventflag){
						SpRend.color = Color.Lerp (Original, RED, BlingDown);
						if (!turnflag) {
								BlingDown += Time.deltaTime * 2;
								if (SpRend.color == RED) {
										turnflag = true;
								}
						} else {
								BlingDown -= Time.deltaTime * 2;
								if (SpRend.color == Original) {
										turnflag = false;
								}
						}
				if(ForceFlag){
					if(SmokeGameManager.eventprog == SmokeGameManager.EVENTPROG.NONE) {
						if (EventManager.Instance)EventManager.Instance.EventStart();
					}

				}

			}else{
				
				SpRend.color = Original;
			}

						
				} else {
			eventflag = false;
			if(preselect){
			if(EventManager.Instance)EventManager.Instance.PriorityZero();
			}
			SpRend.color = Original;
		}
		preselect = selected;		
	}
	public void SetSelected(){
		selected = true;
	}
	public bool EventAreaScan(){
		Vector2 _thisposition = new Vector2 (this.transform.localPosition.x, this.transform.localPosition.y);
		Vector2 StartPosition = _thisposition + EventArea_Center - (Vector2.right * EventArea_Length / 2);
		Vector2 EndPosition = _thisposition + EventArea_Center + (Vector2.right * EventArea_Length / 2);
		bool _eventarea = Physics2D.Linecast(
			StartPosition,
			EndPosition,
			playerLayer
			);
		if (EventAreaVisible) {
			if(_Line!=null){
				_Line.SetPosition(0,new Vector3(StartPosition.x,StartPosition.y,-3));
				_Line.SetPosition(1,new Vector3(EndPosition.x,EndPosition.y,-3));
				_Line.material = new Material(Shader.Find ("Sprites/Default"));
				_Line.SetColors(Color.blue,Color.blue);
				_Line.SetWidth (0.1f, 0.1f);

			}else{
				
				this.gameObject.AddComponent("LineRenderer");
				_Line = GetComponent<LineRenderer>();
				_Line.useWorldSpace = true;
			}

		}

		return _eventarea;
	}
}
