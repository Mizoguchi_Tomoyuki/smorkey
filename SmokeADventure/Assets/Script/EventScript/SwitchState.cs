﻿using UnityEngine;
using System.Collections;

public class SwitchState : MonoBehaviour {
	public bool switchOn;
	public bool switchState;
	public bool receiveflag = false;
	public SwitchEvent _SwitchEvent;

	public enum SwitchType
		{
		SPRITE = 0,
		ANIMATION = 1,

	};
	public SwitchType _switchType;
	public Sprite OffSprite;
	public Sprite OnSprite;
	public SpriteRenderer SpriRen;
	public Animator Anim;
	// Use this for initialization
	void Start () {
		_SwitchEvent = GetComponent<SwitchEvent> ();
		
		switch (_switchType) {
		case SwitchType.SPRITE:
			SpriRen = GetComponent<SpriteRenderer>();
			break;
		case SwitchType.ANIMATION:
			Anim = GetComponent<Animator>();
			break;
		default:
			break;
		}
	}
	
	// Update is called once per frame
	void Update () {
		if (!receiveflag) {
						switchOn = _SwitchEvent.switchState;
				} else {
			switchOn = switchState;
				}
		switch (_switchType) {
		case SwitchType.SPRITE:
			if(switchOn){
				SpriRen.sprite = OnSprite;

			}else{
				SpriRen.sprite = OffSprite;
			}
			break;
		case SwitchType.ANIMATION:
			Anim.SetBool ("SwitchFlag",switchOn);
			break;
		default:
			break;
		}
	
	}
}
