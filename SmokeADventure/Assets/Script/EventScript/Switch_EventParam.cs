﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof (SwitchState))]
[RequireComponent(typeof (EventSet))]
public class Switch_EventParam : MonoBehaviour {
	public SwitchState _SwitchState;
	private EventSet _EventSet;
	public bool EventTypeswitch=false;
	public EventManager.EventType OnEvent,OffEvent;
	public bool ForceSwitch=false;
	public bool PlayerLayerSwitch = false;
	public LayerMask OnPlayerLayer,OffPlayerLayer;
	public bool prioritySwitch = false;
	public int OnPriority,OffPriority;
	// Use this for initialization
	void Start () {
		if (_SwitchState == null) {
						_SwitchState = GetComponent<SwitchState> ();
				}
		_EventSet = GetComponent<EventSet> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (EventTypeswitch) {
						if (_SwitchState.switchState) {
								_EventSet._eventType = OnEvent;

						} else if (!_SwitchState.switchState) {
			
								_EventSet._eventType = OffEvent;

						}
				}
		if (ForceSwitch) {
						_EventSet.ForceFlag = _SwitchState.switchState;
				}
		if (PlayerLayerSwitch) {
			if (_SwitchState.switchState) {
				_EventSet.playerLayer = OnPlayerLayer;
				
			} else if (!_SwitchState.switchState) {
				
				_EventSet.playerLayer = OffPlayerLayer;
				
			}


		}
		if (prioritySwitch) {
			
			if (_SwitchState.switchState) {
				_EventSet.priority = OnPriority;
				
			} else if (!_SwitchState.switchState) {
				
				_EventSet.priority  = OffPriority;
				
			}

				}
	}
}
