﻿using UnityEngine;
using System.Collections;

[System.Serializable]
[RequireComponent(typeof (EventSet))]
public class AnimationsParam {
	public enum EventProg{
		START = 0,
		PLAY = 1,
		END = 2,
	};
	public EventProg animationEventprog = EventProg.START;
	
	[Header("全体時間")]
	public float animationTime;

	[Header("フォーカス")]
	public bool focusflag = false;
	public GameObject _FocusObject;
	public FocusObject.FocusMode focusmode;

	[Header("オート")]
	public bool Auto;

	[Header("選択肢")]
	public bool selectflag = false;
	public int selectStepNumber;
	
	[Header("スイッチ")]
	public bool switchflag = false;
	public GameObject[] SwitchObject;
	private SwitchState[] _SwState;

	[Header("テキスト")]
	public bool Textflag =false;
	public string Text;


	public enum AnimationType{
		INVISIBLE = 0,
		VISIBLE = 1,
		MOVE = 2,
		STAND = 3,
	};
	[Header("アニメーション")]
	public bool Animationflag;
	public GameObject Animation_Object;
	public AnimationType animationType;
	public string AnimationFlagName;
	public GameObject ToMovePoint;
	public float speed = 2;
	private float counter = 0;

	
	[HideInInspector]
	public Camera _MainCamera;
	[HideInInspector]
	public int nowNumber, NextNumber;
	[HideInInspector]
	public CanvasManager _AnimCanv;
	[HideInInspector]
	public Animator _anim;
	[HideInInspector]
	public FocusObject _Focus;

	public void AnimationProcess(){
		switch(animationEventprog){
		case EventProg.START: 
			animationEventprog = EventProg.PLAY;
			if(_MainCamera==null){
				animationEventprog = EventProg.END;
			}else{
				if(focusflag){
					_Focus = _MainCamera.GetComponent<FocusObject>();
					_Focus._Sec = animationTime;
					_Focus.ToFObject = _FocusObject;
					_Focus.ToF = focusmode;
					_Focus.FocusChange = true;
				}
				if(Animationflag){
					_anim = Animation_Object.GetComponent<Animator>();
					_anim.SetBool(AnimationFlagName,true);
				}
				if(Animationflag){
					_anim = Animation_Object.GetComponent<Animator>();
					_anim.SetBool(AnimationFlagName,true);
				}
				if(Textflag){
					_AnimCanv.TextZoneTextSet(Text);
					_AnimCanv.TextZoneAppear();
				}
				if(selectflag){
					
					_AnimCanv.SelecterAppear();
				}
				if(switchflag){
					_SwState = new SwitchState[SwitchObject.Length];
					for(int i=0;i<SwitchObject.Length;i++){
					_SwState[i] = SwitchObject[i].GetComponent <SwitchState>();
					}
				}
			}
			break;
		case EventProg.PLAY:
			counter+=Time.deltaTime;
			if(counter>animationTime){
				if(Auto||Input.GetKey("z")){
					for(int i=0;i<SwitchObject.Length;i++){
						_SwState[i].switchState = true;
					}
					animationEventprog = EventProg.END;
				}
				if(animationType == AnimationType.MOVE){
					animationType = AnimationType.STAND;
				}
			}
			if(Animationflag){
			switch(animationType){
			case AnimationType.INVISIBLE:
				Animation_Object.SetActive (false);
				break;
			case AnimationType.VISIBLE:
				Animation_Object.SetActive (true);
				break;
			case AnimationType.STAND:
				break;
			case AnimationType.MOVE:
				Animation_Object.transform.position = Animation_Object.transform.position + (ToMovePoint.transform.position-Animation_Object.transform.position)*(counter/animationTime);
				break;
			}
			}
			break;
		case EventProg.END:
			if(Animationflag){
				if(_anim != null){
				_anim.SetBool(AnimationFlagName,false);
				_anim.SetBool("returnFlag",true);
				}
			}
				if(Textflag){
				
				_AnimCanv.TextZoneDisAppear();
			}
			if(selectflag){
				if(_AnimCanv.GetSelecterChoice()){
					NextNumber = nowNumber+1;
				}else{
					NextNumber = selectStepNumber;
				}


				_AnimCanv.SelecterDisAppear();
			}else{
				NextNumber = nowNumber+1;
			}
			break;
			
		}
	}
}
[RequireComponent(typeof (EventSet))]
public class AnimationEvent : MonoBehaviour {
	public static int AnimationNum = 0;
	public bool trashflag = true;
	public AnimationsParam[] Animas;
	// Use this for initialization
	void Start () {

	
	}
	
	// Update is called once per frame
	void Update () {
	}

}
