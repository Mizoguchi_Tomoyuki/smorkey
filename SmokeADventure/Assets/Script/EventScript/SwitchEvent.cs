﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof (EventSet))]
[RequireComponent(typeof (SwitchState))]
public class SwitchEvent : MonoBehaviour {
	public string[] Texts;
	public int priority;
	public GameObject SwitchObject;

	public bool selectflag = false;
	public int selectTextNumber = 0;

	public bool switchState = false;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
