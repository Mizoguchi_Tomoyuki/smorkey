﻿using UnityEngine;
using System.Collections;

public class EventManager : SingletonMonoBehaviour<EventManager> {
	public enum EventType
		{
		TEXTREAD =0,
		TEXTSTREAM = 1,
		LAYER=2,
		WARP = 3,
		ITEM = 4,
		SWITCH = 5,
		ANIMATION = 6,

	}
	public float nowPriority = 0;
	public GameObject EventObject;
	public EventSet EventSetter;
	public bool EventisPlaying = false;
	public GameObject PlayerObject;
	public Camera __MainCamera;
	public CanvasManager canvas;
	public BackGroundManager background;
	TextReadEvent TextReadEv;
	private int textCounter = 0;

	
	LayerMoveEvent LayerMoveEv;

	SwitchEvent SwitchEv;
	SwitchState SwitchObjState;

	WarpEvent WarpEv;
	public float WarpCounter = 0;
	public bool warpPlaying = false;
	int num = 0;


	
	AnimationEvent AnimationEv;
	AnimationsParam _animationParam;
	public int animNumber = 0;
	public void Awake()
	{
		if(this != Instance)
		{
			Destroy(this);
			return;
		}
		
		DontDestroyOnLoad(this.gameObject);
		TextReadEv = null;
		canvas.TextZoneDisAppear();
	} 
	public void Update(){
		if (EventisPlaying) {
			switch (EventSetter._eventType){
			case EventType.TEXTREAD:
				Event_TextRead();
				break;
			case EventType.LAYER:
				Event_LayerMove();
				break;
			case EventType.SWITCH:
				Event_SwitchPush();
				break;
			case EventType.WARP:
				Event_WarpPoint();
				break;
			case EventType.ANIMATION:
				Event_Animation();
				break;



			}
		}
	}

	
	public bool EventAdd(float priority,GameObject EventObj){
		bool retbool = false;
		if (nowPriority == 0 || nowPriority <= priority) {
			nowPriority = priority;
			retbool = true;
			EventObject = EventObj;
		}
		return retbool;
		
	}
	public void PriorityZero(){
		EventObject = null;
		nowPriority = 0;

	}
	public void EventStart(){
		if (EventObject != null) {
			Debug.Log ("EventStart");
			SmokeGameManager.eventprog = SmokeGameManager.EVENTPROG.START;
			EventisPlaying = true;
			EventSetter = EventObject.GetComponent<EventSet>();
		}

	}
	public void Event_TextRead (){
		
		switch(SmokeGameManager.eventprog) {
		case SmokeGameManager.EVENTPROG.START:
			Debug.Log ("Event-Start");
			TextReadEv = EventObject.GetComponent<TextReadEvent>();
			textCounter = 0;
			canvas.TextZoneTextSet(TextReadEv.Texts[textCounter]);

			canvas.TextZoneAppear();
			SmokeGameManager.EventProgStepUp();
			break;
		case SmokeGameManager.EVENTPROG.PLAY:
			Debug.Log ("Event-Play");
			if(Input.GetKeyDown("z")){
				if(textCounter+1 >= TextReadEv.Texts.Length){
					SmokeGameManager.EventProgStepUp();
				}else{
					textCounter++;
					canvas.TextZoneTextSet(TextReadEv.Texts[textCounter]);
				}
			}
			break;
		case SmokeGameManager.EVENTPROG.END:
			TextReadEv = null;
			textCounter = 0;
			canvas.TextZoneDisAppear();
			SmokeGameManager.EventProgStepUp();
			break;

				}


	}
	public void Event_LayerMove(){
		
		switch(SmokeGameManager.eventprog) {
		case SmokeGameManager.EVENTPROG.START:
			Debug.Log ("Event-Start");
			LayerMoveEv = EventObject.GetComponent<LayerMoveEvent>();
			textCounter = 0;
			if(LayerMoveEv.Texts.Length>0){
			canvas.TextZoneTextSet(LayerMoveEv.Texts[textCounter]);
			}
			canvas.TextZoneAppear();
			SmokeGameManager.EventProgStepUp();
			break;
		case SmokeGameManager.EVENTPROG.PLAY:
			Debug.Log ("Event-Play");
			if(LayerMoveEv.selectflag){
				if(textCounter == LayerMoveEv.selectTextNumber){
					canvas.SelecterAppear();
					if(Input.GetKeyDown("z")){
						bool select = canvas.GetSelecterChoice();
						if(select){
							background.ChangeBackGroundLayer(LayerMoveEv.moveLayer);
							
							SmokeGameManager.EventProgStepUp();
						}else{
							
							SmokeGameManager.EventProgStepUp();
						}
					}
				}else{
					if(Input.GetKeyDown("z")){
						if(textCounter+1 >= LayerMoveEv.Texts.Length){
							background.ChangeBackGroundLayer(LayerMoveEv.moveLayer);
							SmokeGameManager.EventProgStepUp();
						}else{
							textCounter++;
							canvas.TextZoneTextSet(LayerMoveEv.Texts[textCounter]);
						}
					}
				}
			}else{
				if(Input.GetKeyDown("z")){
					if(textCounter+1 >= LayerMoveEv.Texts.Length){
						background.ChangeBackGroundLayer(LayerMoveEv.moveLayer);
						SmokeGameManager.EventProgStepUp();
					}else{
						textCounter++;
						canvas.TextZoneTextSet(LayerMoveEv.Texts[textCounter]);
					}
				}
			}
			break;
		case SmokeGameManager.EVENTPROG.END:
			LayerMoveEv = null;
			textCounter = 0;
			canvas.SelecterDisAppear();
			canvas.TextZoneDisAppear();
			SmokeGameManager.EventProgStepUp();
			break;
			
		}
		
		
	}

	public void Event_SwitchPush(){

		
		switch(SmokeGameManager.eventprog) {
		case SmokeGameManager.EVENTPROG.START:
			Debug.Log ("Event-Start");
			SwitchEv = EventObject.GetComponent<SwitchEvent>();
			GameObject SwitchObj;
			SwitchObj = SwitchEv.SwitchObject;
			SwitchObjState = SwitchObj.GetComponent<SwitchState>();
			textCounter = 0;
			if(SwitchEv.Texts.Length>0){
				canvas.TextZoneTextSet(SwitchEv.Texts[textCounter]);
				canvas.TextZoneAppear();
			}
			SmokeGameManager.EventProgStepUp();
			break;
		case SmokeGameManager.EVENTPROG.PLAY:
			Debug.Log ("Event-Play");
			if(SwitchEv.selectflag){
				if(textCounter == SwitchEv.selectTextNumber){
					canvas.SelecterAppear();
					if(Input.GetKeyDown("z")){
						bool select = canvas.GetSelecterChoice();
						if(select){
							SwitchObjState.switchState = !SwitchObjState.switchState;
							SwitchEv.switchState = !SwitchEv.switchState;
							
							SmokeGameManager.EventProgStepUp();
						}else{
							
							SmokeGameManager.EventProgStepUp();
						}
					}
				}else{
					if(Input.GetKeyDown("z")){
						if(textCounter+1 >= SwitchEv.Texts.Length){
							
							SwitchObjState.switchState = !SwitchObjState.switchState;
							SwitchEv.switchState = !SwitchEv.switchState;
							SmokeGameManager.EventProgStepUp();
						}else{
							textCounter++;
							canvas.TextZoneTextSet(SwitchEv.Texts[textCounter]);
						}
					}
				}
			}else{
					if(textCounter+1 >= SwitchEv.Texts.Length){
						SwitchObjState.switchState = !SwitchObjState.switchState;
						SwitchEv.switchState = !SwitchEv.switchState;
						SmokeGameManager.EventProgStepUp();
					}else{
					if(Input.GetKeyDown("z")){
						textCounter++;
						canvas.TextZoneTextSet(SwitchEv.Texts[textCounter]);
					}
				}
			}
			break;
		case SmokeGameManager.EVENTPROG.END:
			SwitchEv = null;
			textCounter = 0;
			canvas.SelecterDisAppear();
			canvas.TextZoneDisAppear();
			SmokeGameManager.EventProgStepUp();
			break;
			
		}
		
		
	}

	
	public void Event_WarpPoint(){
		
		
		switch(SmokeGameManager.eventprog) {
		case SmokeGameManager.EVENTPROG.START:
			Debug.Log ("Event-Start");
			WarpEv = EventObject.GetComponent<WarpEvent>();
			textCounter = 0;
			if(WarpEv.Texts.Length>0){
				canvas.TextZoneTextSet(WarpEv.Texts[textCounter]);
				canvas.TextZoneAppear();
			}
			if(!WarpEv.Playervisible){
				PlayerControll PLCon = PlayerObject.GetComponent<PlayerControll>();
				PLCon.PlayerInVisible();
			}
			SmokeGameManager.EventProgStepUp();
			break;
		case SmokeGameManager.EVENTPROG.PLAY:
			Debug.Log ("Event-Play");
			if(WarpEv.selectflag){
				if(textCounter == WarpEv.selectTextNumber){
					canvas.SelecterAppear();
					if(Input.GetKeyDown("z")){
						bool select = canvas.GetSelecterChoice();
						if(select){
							warpPlaying = true;

						}else{
							
							SmokeGameManager.EventProgStepUp();
						}
					}
				}else{
					if(Input.GetKeyDown("z")){
						if(textCounter+1 >= WarpEv.Texts.Length){
							
							warpPlaying = true;
						}else{
							textCounter++;
							canvas.TextZoneTextSet(WarpEv.Texts[textCounter]);
						}
					}
				}
			}else{
					if(textCounter+1 >= WarpEv.Texts.Length){
						warpPlaying = true;
					}else{
					if(Input.GetKeyDown("z")){
						textCounter++;
						canvas.TextZoneTextSet(WarpEv.Texts[textCounter]);
					}
				}
			}
			if(warpPlaying){
				WarpCounter += Time.deltaTime;
				if(WarpEv.MoveFlag){
					Vector2 PlPosition2D = new Vector2(PlayerObject.transform.position.x,PlayerObject.transform.position.y);
					Vector2 WaEvPosition2D = new Vector2(WarpEv.WarpPoint[num].transform.position.x,WarpEv.WarpPoint[num].transform.position.y);
					Vector2 DireVector =  PlPosition2D - WaEvPosition2D;
					PlayerObject.transform.position = new Vector3(PlPosition2D.x-DireVector.x*(WarpCounter/WarpEv.WarpDelay),
					                                              PlPosition2D.y-DireVector.y*(WarpCounter/WarpEv.WarpDelay),
					              								PlayerObject.transform.position.z);
				//	float distance = Vector2.Distance(PlPosition2D,WaEvPosition2D);
					if(WarpCounter>WarpEv.WarpDelay){
						num++;
						WarpCounter = 0;
						if(num>=WarpEv.WarpPoint.Length){
							SmokeGameManager.EventProgStepUp();
						}
					}
				}else{
				if(WarpCounter>WarpEv.WarpDelay){
					WarpCounter = 0;
					PlayerObject.transform.position = WarpEv.WarpPoint[num].transform.position;
					num++;
					Debug.Log (num);
					if(num>=WarpEv.WarpPoint.Length){
						SmokeGameManager.EventProgStepUp();
					}
				}
				}

			}
			break;
		case SmokeGameManager.EVENTPROG.END:
			if(!WarpEv.Playervisible){
				PlayerControll PLCon = PlayerObject.GetComponent<PlayerControll>();
				PLCon.PlayerVisible();
			}
			WarpEv = null;
			textCounter = 0;
			num = 0;
			warpPlaying = false;
			canvas.SelecterDisAppear();
			canvas.TextZoneDisAppear();
			SmokeGameManager.EventProgStepUp();
			break;
			
		}
		
		
	}

	
	public void Event_Animation(){
		
		
		switch(SmokeGameManager.eventprog) {
		case SmokeGameManager.EVENTPROG.START:
			AnimationEv = EventObject.GetComponent<AnimationEvent>();
			if(AnimationEv.Animas.Length>0){
				animNumber = 0;
				_animationParam = AnimationEv.Animas[animNumber];
				AnimationEv.Animas[animNumber].nowNumber = animNumber;
				AnimationEv.Animas[animNumber]._MainCamera = __MainCamera;
				AnimationEv.Animas[animNumber]._AnimCanv = canvas;
			}
			Debug.Log ("Event-Start");

			SmokeGameManager.EventProgStepUp();
			break;
		case SmokeGameManager.EVENTPROG.PLAY:
			Debug.Log ("Event-Play");
			AnimationEv.Animas[animNumber].AnimationProcess();
			if(AnimationEv.Animas[animNumber].animationEventprog == AnimationsParam.EventProg.END){
				animNumber = AnimationEv.Animas[animNumber].NextNumber;
				if(AnimationEv.Animas.Length>animNumber){
				AnimationEv.Animas[animNumber]._MainCamera = __MainCamera;
					AnimationEv.Animas[animNumber]._AnimCanv = canvas;
					AnimationEv.Animas[animNumber].nowNumber = animNumber;
				}
				Debug.Log (animNumber);
			}
			if(AnimationEv.Animas.Length<=animNumber){
				if(AnimationEv.trashflag){
					EventObject.SetActive(false);
				}
				SmokeGameManager.EventProgStepUp();

			}
			break;
		case SmokeGameManager.EVENTPROG.END:
			Debug.Log ("Event-End");
			AnimationEv = null;
			animNumber = 0;
			_animationParam = null;
			SmokeGameManager.EventProgStepUp();
			break;
			
		}
		
		
	}
}
