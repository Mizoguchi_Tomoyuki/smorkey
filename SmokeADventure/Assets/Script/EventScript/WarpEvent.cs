﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof (EventSet))]
public class WarpEvent : MonoBehaviour {
	public string[] Texts;
	public int priority;
	
	public bool selectflag = false;
	public int selectTextNumber = 0;
	public GameObject[] WarpPoint;
	public float WarpDelay;
	public bool MoveFlag;
	public bool Playervisible= true;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
