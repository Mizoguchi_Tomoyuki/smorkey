﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof (EventSet))]
public class LayerMoveEvent : MonoBehaviour {
	public string[] Texts;
	public int priority;

	public bool selectflag = false;
	public int selectTextNumber = 0;
	public int moveLayer = 0;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
