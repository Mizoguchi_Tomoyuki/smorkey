﻿using UnityEngine;
using System.Collections;

public static class SmokeGameManager{
	public static GameObject eventObj;
	public enum EVENTPROG {
		NONE =0,
		START =1,
		PLAY = 2,
		END = 3,

	};
	public static EVENTPROG eventprog = EVENTPROG.NONE;
	static SmokeGameManager(){
		Init ();
	}

	public static void Init(){
		eventprog = EVENTPROG.NONE;
	}

	public static void SetEventFlag(EVENTPROG ep){
		eventprog = ep;
	}

	public static void EventAddObj(GameObject Obj){
		eventObj = Obj;
	}
	public static void EventProgStepUp(){
		switch (eventprog) {
		case EVENTPROG.NONE:
			eventprog = EVENTPROG.START;
			break;
		case EVENTPROG.START:
			eventprog = EVENTPROG.PLAY;
			break;
		case EVENTPROG.PLAY:
			eventprog = EVENTPROG.END;
			break;
		case EVENTPROG.END:
			eventprog = EVENTPROG.NONE;
			break;


				}

	}

}
