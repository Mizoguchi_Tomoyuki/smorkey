﻿using UnityEngine;
using System.Collections;

public class PlayerControll : MonoBehaviour {
	public float highspeed = 0.1f;
	public float lowspeed = 0.03f;
	private float speed = 0.1f;
	public PlayerState _PlayerState;
	public float smokeCount = 0;
	private float UpSpeed = 240;
	public float inertia = 0;
	private float inertia_decrementer = 0;
	private bool squateFlag = false;
	public PlayerSpriteChanger _playerspriteChanger;
	public GameObject Smoke_particle;
	public GameObject PlayerGraphics;
	private BoxCollider2D _hit;
	// Use this for initialization
	void Start () {
		if (_PlayerState == null) {
			_PlayerState = this.GetComponent<PlayerState>();
		}
		_hit = GetComponent<BoxCollider2D> ();
	}
	
	// Update is called once per frame
	void Update () {
		//必要なデータ入力の収集.  
		float horizontal_ax = Input.GetAxis("Horizontal");
		PlayerState.PLAYER_STATE pl_state = _PlayerState.player_state;
		PlayerState.PLAYER_ACTION pl_action = _PlayerState.player_action;

		if (Input.GetKeyDown ("space") && pl_state != PlayerState.PLAYER_STATE.EVENT) {
			if(pl_action == PlayerState.PLAYER_ACTION.SMOKE){
				rigidbody2D.gravityScale = 1.0f;
				smokeCount = 0;
				if(!squateFlag){
					_PlayerState.SetPlayer_Action(PlayerState.PLAYER_ACTION.NORMAL);
					_playerspriteChanger.ChangeAnim("Return");
				}else{
					squateFlag = false;
					_PlayerState.SetPlayer_Action(PlayerState.PLAYER_ACTION.SQUATE);
				}
			}else{
				inertia = horizontal_ax;
				if(inertia>0){
					inertia_decrementer = 0.01f;
				}else{
					inertia_decrementer = -0.01f;
				}
				rigidbody2D.gravityScale = 0.0f;
				if(pl_action == PlayerState.PLAYER_ACTION.SQUATE){
					squateFlag = true;
				}else{
					
					squateFlag = false;
				}
				_PlayerState.SetPlayer_Action(PlayerState.PLAYER_ACTION.SMOKE);
				GameObject smoke_ = (GameObject)Instantiate(Smoke_particle,this.transform.position,Quaternion.identity);
				smoke_.transform.parent = this.transform;
				_playerspriteChanger.ChangeAnim("Smoke");
			}
		}
		if (Input.GetKeyDown ("z") && SmokeGameManager.eventprog == SmokeGameManager.EVENTPROG.NONE) {
			if (EventManager.Instance)EventManager.Instance.EventStart();
		}
		//stateに対する動きの処理. 
		switch (pl_state) {
		case PlayerState.PLAYER_STATE.FLOAT:
			speed =lowspeed;
			break;
		case PlayerState.PLAYER_STATE.GROUND:
			speed =highspeed;
			break;
		case PlayerState.PLAYER_STATE.BLOW:
			speed =highspeed;
			break;
		case PlayerState.PLAYER_STATE.EVENT:
			speed = 0;
			horizontal_ax = 0;
			break;
		}
		//actionに対する処理. 
		if (pl_state != PlayerState.PLAYER_STATE.EVENT) { //Event中はアクションにかかわることは無効化. 

						switch (pl_action) {
						case PlayerState.PLAYER_ACTION.NORMAL:
								this.gameObject.layer = 8;
								if (Mathf.Abs (horizontal_ax) > 0 && !_PlayerState.bodydown && _PlayerState.bodyup) {
										_PlayerState.SetPlayer_Action (PlayerState.PLAYER_ACTION.SQUATE);
								}
								break;
						case PlayerState.PLAYER_ACTION.SMOKE:
								this.gameObject.layer = 11;
								smokeCount += Time.deltaTime;
								speed = 0f;
								float smokeupper = Mathf.Sin ((smokeCount * UpSpeed / 180) * Mathf.PI) + 0.8f;
								inertia -= smokeCount * inertia_decrementer;
								if (inertia * inertia_decrementer <= 0) {
										inertia = 0;
								}
								transform.Translate (Vector3.up * smokeupper * 0.01f +
										Vector3.right * inertia * 0.1f);
								if (smokeCount > 10) {
										rigidbody2D.gravityScale = 1.0f;
										smokeCount = 0;
										if (!squateFlag) {
						_PlayerState.SetPlayer_Action (PlayerState.PLAYER_ACTION.NORMAL);
						_playerspriteChanger.ChangeAnim("Return");
										} else {
												squateFlag = false;
												_PlayerState.SetPlayer_Action (PlayerState.PLAYER_ACTION.SQUATE);
										}
								}
								break;
						case PlayerState.PLAYER_ACTION.SQUATE:
							this.gameObject.layer = 8;
								speed = lowspeed;
								if (!_PlayerState.bodydown && !_PlayerState.bodyup && pl_action == PlayerState.PLAYER_ACTION.SQUATE) {//立ち状態に戻す処理. 
										_PlayerState.SetPlayer_Action (PlayerState.PLAYER_ACTION.NORMAL);
								}
								break;
						default:
								break;
						}

				}



			transform.Translate (Vector3.right * horizontal_ax * speed);
		//振り向き判定. 
		if (horizontal_ax > 0) {
						_PlayerState.player_forward = true;
		} else if (horizontal_ax < 0){
			_PlayerState.player_forward = false;

				}

	}
	
	public void PlayerInVisible(){
		_hit.enabled = false;
		PlayerGraphics.GetComponent<SpriteRenderer>().enabled = false;
	}
	public void PlayerVisible(){
		_hit.enabled = true;
		PlayerGraphics.GetComponent<SpriteRenderer>().enabled = true;
		
	}
}
