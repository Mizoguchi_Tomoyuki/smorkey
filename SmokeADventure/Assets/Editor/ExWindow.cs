﻿using UnityEngine;
using UnityEditor; // エディタ拡張関連はUnityEditor名前空間に定義されているのでusingしておく。
using System.Collections;

// エディタに独自のウィンドウを作成する
public class ExWindow : EditorWindow
{
	//GameObject Editorcamera;
	Camera _EditorCamera = Camera.main;
	RenderTexture renderTexture;
	// メニューのWindowにEditorExという項目を追加。
	[MenuItem("Window/EditorEx")]
	static void Open()
	{
		// メニューのWindow/EditorExを選択するとOpen()が呼ばれる。
		// 表示させたいウィンドウは基本的にGetWindow()で表示＆取得する。
		EditorWindow editorWindow=GetWindow<ExWindow>( "2DEvent" ); // タイトル名を"EditorEx"に指定（後からでも変えられるけど）
		editorWindow.autoRepaintOnSceneChange = true;
		editorWindow.Show();
	}
	public void Awake(){
		
		renderTexture = new RenderTexture((int)position.width, 
		                                  (int)position.height, 
		                                  (int)RenderTextureFormat.ARGB32 );
		}
	// Windowのクライアント領域のGUI処理を記述
	int leftSize = 10;
	Vector2 leftScrollPos = Vector2.zero;
	int rightSize = 10;
	Vector2 rightScrollPos = Vector2.zero;
	EventManager.EventType _events;
	void OnGUI()
	{
		EditorGUILayout.LabelField( "EventSetの変更を行えます" ); // いつまでも居座り続けるぜ！


		//左側UI-----------------------------------------
		EditorGUILayout.BeginHorizontal( GUI.skin.box );
		
		EditorGUILayout.BeginVertical( GUI.skin.box,GUILayout.Width ( 300 ) );
		EditorGUILayout.LabelField( "EventSet" );
		
		GameObject[] objs = Selection.gameObjects;
		EventSet evs;
		int forbidnum = 0;
		bool forbiddFlag = true;
		bool editflag = true;
		if (objs.Length > 1) {
			foreach (GameObject Go in objs) {
				if (Go.GetComponent<EventSet>() != null) {
					forbidnum++;
				}
			}
			if(forbidnum == objs.Length){
				forbiddFlag = false;
			}else if(forbidnum == 0){
				forbiddFlag = false;
			}else{
				forbiddFlag = true;
			}
			
		}else{
			editflag = false;
			evs = objs[0].GetComponent<EventSet>();
			forbiddFlag = false;
		}
	//	leftSize = EditorGUILayout.IntSlider ( "Size",leftSize,10,100,GUILayout.ExpandWidth(false) );
		
		// 左側のスクロールビュー(横幅300px)
		leftScrollPos = EditorGUILayout.BeginScrollView( leftScrollPos,GUI.skin.box,GUILayout.Height(100));
		{
			EditorGUI.BeginDisabledGroup(forbiddFlag);
			_events = (EventManager.EventType)EditorGUILayout.EnumPopup( "EventType",(System.Enum)_events ); //EventTypeを指定.
			
			EditorGUI.EndDisabledGroup();
		}
		EditorGUILayout.EndScrollView();
		EditorGUI.BeginDisabledGroup(forbiddFlag);
		if (objs.Length > 1) {
			if (forbidnum > 0) {
				if (GUILayout.Button ("ApplyEvent")) {
					Debug.Log ("Apply-Event");
					ApplyEvent(_events);
					
				}
			} else {
				if (GUILayout.Button ("SetEvent")) {
					Debug.Log ("Set-Event");
					AddEvent (_events);
					
					
				}
			}
		} else if (objs.Length > 0) {
			if (objs [0].GetComponent<EventSet> () == null) {
				if (GUILayout.Button ("SetEvent")) {
					Debug.Log ("Set-Event");
					AddEvent (_events);
				}
			} else {
				if (GUILayout.Button ("ApplyEvent")) {
					Debug.Log ("Apply-Event");
					ApplyEvent(_events);
					
				}
			}
		}
		
		EditorGUI.EndDisabledGroup();
		leftScrollPos = EditorGUILayout.BeginScrollView( leftScrollPos,GUI.skin.box);
		EditorGUI.BeginDisabledGroup(editflag);
		//////////////////////////////////////////////////////////////////////////////////////////////////////////
		EditorGUI.EndDisabledGroup();
		EditorGUILayout.EndScrollView();


		GUILayout.Box((Texture)renderTexture,GUI.skin.box,GUILayout.Width(320),GUILayout.Height(180));
		EditorGUILayout.BeginHorizontal();
		if (GUILayout.Button ("SetPosition",GUILayout.Width(160))) {
		}
		if (GUILayout.Button ("Animation",GUILayout.Width(160))) {
		}
		EditorGUILayout.EndHorizontal();
		EditorGUILayout.EndVertical();


		//左側UIの終着------------------------------------------


		EditorGUILayout.BeginVertical( GUI.skin.box );
		
		EditorGUILayout.LabelField( "右側" );
		
		rightSize = EditorGUILayout.IntSlider ( "Size",rightSize,10,100,GUILayout.ExpandWidth(false) );
		
		// 右側のスクロール
		rightScrollPos = EditorGUILayout.BeginScrollView( rightScrollPos,GUI.skin.box );
		{
			// スクロール範囲
			
			for( int y = 0;y<rightSize;y++ )
			{
				EditorGUILayout.BeginHorizontal( GUI.skin.box );
				{
					// ここの範囲は横並び
					
					EditorGUILayout.PrefixLabel( "Index " + y );
					
					// 下に行くほどボタン数増やす
					for( int i=0;i<y+1;i++ )
					{
						// ボタン(横幅100px)
						if( GUILayout.Button("Button"+i,GUILayout.Width(100) ) )
						{

						}
					}
				}
				EditorGUILayout.EndHorizontal();
			}
			// こんな感じで横幅固定しなくても、範囲からはみ出すときにスクロールバー出してくれる。
		}
		EditorGUILayout.EndScrollView();
		
		//GUI.DrawTexture( new Rect(  position.width/3f, position.height/3f, position.width, position.height), renderTexture );
		EditorGUILayout.EndVertical();
		
		EditorGUILayout.EndHorizontal();
	}
	public void Update() {
		if (renderTexture == null) {
			
			renderTexture = new RenderTexture((int)position.width, 
			                                  (int)position.height, 
			                                  (int)RenderTextureFormat.ARGB32 );
				}
		if(_EditorCamera != null) {
			_EditorCamera.targetTexture = renderTexture;
			_EditorCamera.Render();
			_EditorCamera.targetTexture = null;	
		}
		if(renderTexture.width != position.width || renderTexture.height != position.height)
			renderTexture = new RenderTexture((int)position.width, 
			                                  (int)position.height, 
			                                  (int)RenderTextureFormat.ARGB32 );
	}
	static void AddEvent(EventManager.EventType ev){
		GameObject[] objs = Selection.gameObjects;
				foreach (GameObject go in objs) {
			go.AddComponent("EventSet");
			go.GetComponent<EventSet>()._eventType = ev;
			switch (ev) {
						case EventManager.EventType.WARP:
								go.AddComponent ("WarpEvent");
								break;
						case EventManager.EventType.ANIMATION:
								go.AddComponent ("AnimationEvent");
								break;
						case EventManager.EventType.ITEM:
			//go.AddComponent("AnimationEvent");
								break;
						case EventManager.EventType.LAYER:
								go.AddComponent ("LayerMoveEvent");
								break;
						case EventManager.EventType.SWITCH:
								go.AddComponent ("SwitchEvent");
								go.AddComponent ("Switch_EventParam");
								break;
						case EventManager.EventType.TEXTREAD:
								go.AddComponent ("TextReadEvent");
								break;
						case EventManager.EventType.TEXTSTREAM:
			//	go.AddComponent("LayerMoveEvent");
								break;
						default:
								break;
			
						}
				}
		}
	
	static void ApplyEvent(EventManager.EventType _eventType){
		GameObject[] objs = Selection.gameObjects;
		foreach (GameObject go in objs) {

			switch (go.GetComponent<EventSet>()._eventType) {
			case EventManager.EventType.WARP:
				DestroyImmediate(go.GetComponent<WarpEvent>());
				break;
			case EventManager.EventType.ANIMATION:
				DestroyImmediate(go.GetComponent<AnimationEvent>());
				break;
			case EventManager.EventType.ITEM:
				//go.AddComponent("AnimationEvent");
				break;
			case EventManager.EventType.LAYER:
				DestroyImmediate(go.GetComponent<LayerMoveEvent>());
				break;
			case EventManager.EventType.SWITCH:
				DestroyImmediate(go.GetComponent<SwitchEvent>());
				DestroyImmediate(go.GetComponent<Switch_EventParam>());
				break;
			case EventManager.EventType.TEXTREAD:
				DestroyImmediate(go.GetComponent<TextReadEvent>());
				break;
			case EventManager.EventType.TEXTSTREAM:
				//	go.AddComponent("LayerMoveEvent");
				break;
			default:
				break;
			}

			switch (_eventType) {
			case EventManager.EventType.WARP:
				go.AddComponent ("WarpEvent");
				break;
			case EventManager.EventType.ANIMATION:
				go.AddComponent ("AnimationEvent");
				break;
			case EventManager.EventType.ITEM:
				//go.AddComponent("AnimationEvent");
				break;
			case EventManager.EventType.LAYER:
				go.AddComponent ("LayerMoveEvent");
				break;
			case EventManager.EventType.SWITCH:
				go.AddComponent ("SwitchEvent");
				go.AddComponent ("Switch_EventParam");
				break;
			case EventManager.EventType.TEXTREAD:
				go.AddComponent ("TextReadEvent");
				break;
			case EventManager.EventType.TEXTSTREAM:
				//	go.AddComponent("LayerMoveEvent");
				break;
			default:
				break;
				
			}

			go.GetComponent<EventSet>()._eventType = _eventType;

		}
	}
	static void EventSetDataSet(EventSet setev, GameObject setobj){
	

	}


}