﻿using UnityEngine;
using System.Collections;

public class FocusObject : MonoBehaviour {
	public GameObject FocusObj;
	public enum FocusMode{
		NEAR = 0,
		NORMAL = 1,
		FAR = 2,

	}
	public Vector3 Normalposition, Nearposition, Farposition;
	public FocusMode _focusMode = FocusMode.NORMAL; 
	private Camera _MainCam;
	private GameObject FromObject,ToObject;
	private float Sec;
	public bool FocusChange;
	public GameObject ToFObject;
	public FocusMode ToF;
	public float _Sec = 2f;
	private float clock;
	private float Subclock;
	private bool Moving;
	// Use this for initialization
	void Start () {
		_MainCam = GetComponent<Camera> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (!Moving) {
						Vector3 positions = Vector3.zero;
						switch (_focusMode) {
						case FocusMode.NEAR:
								_MainCam.orthographicSize = 2.5f;
								positions = Nearposition;
								break;
						case FocusMode.NORMAL:
								_MainCam.orthographicSize = 5;
								positions = Normalposition;
								break;
						case FocusMode.FAR:
								_MainCam.orthographicSize = 7f;
								positions = Farposition;
								break;

						}
						if (FocusObj != null) {
								this.transform.position = FocusObj.transform.position + positions;
						}
						if (FocusChange) {
								MoveFocus ();
						}
				} else {
				//	clock += Time.deltaTime;
					Subclock += Time.deltaTime;
			clock += Mathf.Sin((Subclock/_Sec)*Mathf.PI/4);
					Vector3 positions_move = Vector3.zero;
			Vector3 frompositions = Vector3.zero;
			Vector3 topositions= Vector3.zero;
			float toSize=0,fromSize=0;
					switch (ToF) {
				
					case FocusMode.NEAR:
						toSize=2.5f;
						topositions = Nearposition;
						break;
			case FocusMode.NORMAL:
				toSize=5f;
						topositions = Normalposition;
						break;
			case FocusMode.FAR:
				toSize=7f;
						topositions = Farposition;
						break;
					}
					switch (_focusMode) {
				
			case FocusMode.NEAR:
				fromSize=2.5f;
						frompositions = Nearposition;
						break;
			case FocusMode.NORMAL:
				fromSize=5f;
						frompositions = Normalposition;
						break;
			case FocusMode.FAR:
				fromSize=7f;
						frompositions = Farposition;
						break;
					}

			this.transform.position = FocusObj.transform.position+(ToFObject.transform.position-FocusObj.transform.position)*(clock/_Sec)+frompositions + (topositions-frompositions)*(clock/_Sec);
			_MainCam.orthographicSize = fromSize + (toSize-fromSize)*(clock/_Sec);

			if(clock > _Sec){
				Moving = false;
				clock = 0;
				Subclock = 0;
				_focusMode = ToF;
				FocusObj = ToFObject;
				ToFObject = null;
				_Sec = 4;
				ToF = FocusMode.NORMAL;
					}

				}
	}
	public void MoveFocus(){
		clock = 0;
		Subclock = 0;
		if (ToFObject != null) {
						Moving = true;
						FocusChange = false;
				}
	}
}
