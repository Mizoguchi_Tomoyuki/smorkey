﻿using UnityEngine;
using System.Collections;

public class TextureChanger : MonoBehaviour {
	public enum TEXTURE_COLOR
	{
		ROOM = 1,
		KITCHEN = 0,
	}
	public Vector2 TextureSize;
	private int textureWidth, textureHeight;
	public int textureWholeWidth, textureWholeHeight;
	public TEXTURE_COLOR _RoomWall;
	private Material mat;
	public float CountTime = 2;
	private float counter = 0;
	private bool dimor=false;
	// Use this for initialization
	void Start () {
		mat = renderer.material;
		Texture texture = mat.mainTexture;
		textureWidth = textureWholeWidth;
		
		textureHeight = textureWholeHeight;
	}
	
	// Update is called once per frame
	void Update () {
		Vector2 offset, scale;
		
		offset = new Vector2(  0,( TextureSize.y*(int)_RoomWall) / textureHeight);
		
		scale = new Vector2( 61f/99f*this.transform.localScale.x,  TextureSize.y/ textureHeight);
		
		mat.SetTextureOffset("_MainTex", offset);
		
		mat.SetTextureScale("_MainTex", scale);
	}
}
